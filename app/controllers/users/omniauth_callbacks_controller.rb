class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  before_action :fetch_auth_data, only: %i[facebook twitter google_oauth2]

  def facebook
    if @user.persisted?
      set_flash_message(:notice, :success, kind: 'Facebook')
      return sign_in_and_redirect @user
    end
    session['devise.facebook_data'] = request.env['omniauth.auth']['info']['email']
    flash[:info] = 'The email you use with Facebook has already existed'
    redirect_to new_user_registration_url
  end

  def twitter
    if @user.persisted?
      set_flash_message(:notice, :success, kind: 'Twitter')
      return sign_in_and_redirect @user
    end
    session['devise.twitter_data'] = request.env['omniauth.auth']['extra']['raw_info']['email']
    flash[:info] = 'The email you use with Twitter has already existed'
    redirect_to new_user_registration_url
  end

  def google_oauth2
    if @user.persisted?
      set_flash_message(:notice, :success, kind: 'Google')
      return sign_in_and_redirect @user
    end
    session['devise.google_data'] = request.env['omniauth.auth']['info']['email']
    flash[:info] = 'The email you use with Google has already existed'
    redirect_to new_user_registration_url
  end

  def failure
    redirect_to root_path
  end

  protected

  def fetch_auth_data
    @user = OauthProcesses.find_or_create_from_auth_data(request.env['omniauth.auth'])
  end
end
