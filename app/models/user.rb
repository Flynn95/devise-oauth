class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, omniauth_providers: [:facebook, :twitter, :google_oauth2]

  def self.new_with_session(params, session)
    super.tap do |user| # receive session from Devise to init a user
      if (data = (session['devise.facebook_data']) ||
                  (session['devise.twitter_data']) ||
                  (session['devise.google_data']))
        user.email = data['email'] if user.email.blank?
      end
    end
  end
end
