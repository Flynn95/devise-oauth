# frozen_string_literal: true

class OauthProcesses
  def self.find_or_create_from_auth_data(auth)
    User.where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
      user.password = Devise.friendly_token[0, 20]
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = if auth.provider == 'twitter'
                     auth.extra.raw_info.email
                   else
                     auth.info.email
                   end
    end
  end
end
