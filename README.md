# Devise - OAuth

A template using Devise and Omniauth to fetch data from Twitter, Facebook or Google to create/login user.

## Install

Clone this repo and run:

```ruby
bundle install
rails db:migrate
```
